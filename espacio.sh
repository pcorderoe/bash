#!/bin/bash
# -*- ENCODING: UTF-8 -*-

if [ "$USER" = "root" ]; then
	echo "Ingrese el dominio a crear:"
	read "dominio"
	echo "Ingrese el usuario global de la cuenta:"
	read "usuario"
	#falta agregar validaciones de dominio
	#creamos usuario y su carpeta /home/usuario
#	adduser "$usuario"

	mkdir /home/"$usuario"
	useradd -d /home/"$usuario" -s /bin/false "$usuario"
	
	passwd "$usuario"

	mkdir /home/"$usuario"/public
	cp /home/.cfg/index.php /home/"$usuario"/public/
	chown -R "$usuario":"$usuario" /home/"$usuario"/public

	#Esta seguro de continuar?
	echo "/*******Creacion de VHOSTS************/"
	echo "Esta seguro de continuar? (Yes/No)?"
	read "answer"
	if [ "$answer" != "Yes" ] && [ "$answer" != "Y" ] && [ "$answer" != "y" ]; then 
		echo "Deshaciendo las acciones realizadas..."
		rm -R /home/"$usuario"/
		deluser "$usuario"
		exit
	fi
	cp /etc/apache2/sites-available/plantilla /etc/apache2/sites-available/"$dominio"
	#definimos los parametros para nuestro dominio
	sed -i s/dominio/"$dominio"/g /etc/apache2/sites-available/"$dominio"
	sed -i s/usuario/"$usuario"/g /etc/apache2/sites-available/"$dominio"

	echo "Activar VHOST? (y/n)"
	read "r"
	if [ "$r" != "Y" ] && [ "$r" != "y" ]; then
		echo "Deshaciendo las acciones realizadas..."
		rm /etc/apache2/sites-available/"$dominio"
		rm -R /home/"$usuario"
		deluser "$usuario"
		exit
	fi
	cd /etc/apache2/sites-available/
	a2ensite "$dominio"
	service apache2 reload

	echo "Se habilitara el acceso por ftp. Desea continuar? (Y/n)"
	read "ftp"
	if [ "$ftp" != "Y" ] && [ "$ftp" != "y" ]; then
		echo "Deshaciendo las acciones realizadas..."
		rm /etc/apache2/sites-available/"$dominio"
		rm -R /home/"$usuario"
		deluser "$usuario"
		cd /etc/apache2/sites-available/
		a2dissite "$dominio"
		service apache2 reload
		echo "Los cambios fueron eliminados exitosamente."
	fi
	#habilitar cuenta ftp
	cp /etc/proftpd/usuario/plantilla.login /etc/proftpd/conf.d/"$usuario".login
	sed -i s/plantilla/"$usuario"/g /etc/proftpd/conf.d/"$usuario".login

	echo "El virtualhost fue creado exitosamente"
else
	echo "Para crear espacios de desarrollo debes de ser usuario root"
fi

exit
