#!/bin/bash
# -*- ENCODING: UTF-8 -*-

if [ "$USER" = "root" ]; then
	echo "Ingrese el dominio a eliminar:"
	read "dominio"
	echo "Ingrese el usuario global de la cuenta:"
	read "usuario"
	#falta agregar validaciones de dominio
	#creamos usuario y su carpeta /home/usuario
	echo "Esta seguro de continuar? (Yes/No)?"
	read "answer"
	if [ "$answer" == "Yes" ] || [ "$answer" == "Y" ] || [ "$answer" == "y" ]; then 
		cd /etc/apache2/sites-enabled/
		a2dissite "$dominio"
		rm /etc/apache2/sites-available/"$dominio"
		echo "Eliminando cuenta..."
		rm /etc/proftpd/conf.d/"$usuario".login
		rm -R /home/"$usuario"/
		deluser "$usuario"
		service apache2 reload
		echo "Se elimino el espacio"
		exit
	else
		echo "error"
	fi
else
	echo "Para eliminar espacios de desarrollo debes de ser usuario root"
fi

exit
